module gitlab.com/thorchain/midgard

go 1.15

require (
	github.com/99designs/gqlgen v0.12.2
	github.com/getkin/kin-openapi v0.26.0
	github.com/google/go-cmp v0.5.0 // indirect
	github.com/jackc/pgx/v4 v4.8.1
	github.com/jarcoal/httpmock v1.0.6
	github.com/julienschmidt/httprouter v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/onsi/ginkgo v1.10.3 // indirect
	github.com/onsi/gomega v1.7.1 // indirect
	github.com/pascaldekloe/metrics v1.2.0
	github.com/pascaldekloe/sqltest v0.1.0
	github.com/stretchr/testify v1.5.1
	github.com/tendermint/go-amino v0.15.1 // indirect
	github.com/tendermint/tendermint v0.33.4
	github.com/vektah/gqlparser/v2 v2.0.1
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
	google.golang.org/genproto v0.0.0-20191007204434-a023cd5227bd // indirect
)
